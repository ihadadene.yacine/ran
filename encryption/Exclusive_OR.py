# try to dev AES Algorithme

# Check Encryption used in LTE between the sequence and the packet
sequence_N = B'01010'
packet = B'01010'

def xor(ba1,ba2):
    RES = [_a ^ _b for _a, _b in zip(ba1, ba2)]
    return bytes(''.join(map(str,RES)),encoding="raw_unicode_escape")

print(xor(b'0100',b'1000'))

print(xor(xor(b'0100',b'1000'),b'1000'))


